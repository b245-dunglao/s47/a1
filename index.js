/*console.log("JS DOM - Manipulation");


[SECTION] : Document Object Model(DOM)

	- allows us to access or modify the properties of the html element in a webpage
	- it is standard on how to get, change , add or delete html elements
	- we will be focusing only with DOM in terms of managing forms. 

	For selecting HTML element we will be using document.querySelector/getElementById
		Syntax : document.querySelector("html element")

		CSS selectors
			- class selector (.);
			- id selector (#);
			- tag sekectir (html tags)
			- universal selector (*)
			- attribute selector ([attribute])

*/

let universalSelector = document.querySelectorAll("*");
let singleUniversalSelector = document.querySelector("*");

console.log(universalSelector);
console.log(singleUniversalSelector);

let classSelector = document.querySelectorAll(".full-name");
console.log(classSelector);	

let singleClassSelector = document.querySelector(".full-name");
console.log(classSelector);	

let tagSelector = document.querySelectorAll("input");
console.log(tagSelector);	

let spanSelector = document.querySelector("span[id]");
console.log(spanSelector);	

//getElement

let element = document.getElementById("fullName");
console.log(element);	

element = document.getElementsByClassName("full-name");
console.log(element);	

// [SECTION] Event Listeners
	// Whenever a user interacts with a webpage, this action is considered as an event 
	// working with events is large part of creating interactivity in a webpage
	// specific function that will be triggered if the event happen. 

	// the function use is "addEventListener", it takes two arguments
		// first argument a string identifying the event
		// second argument, function that the listener will trigger once the "specified event" occur.


let txtFirstName = document.querySelector("#txt-first-name");

// add event listener
txtFirstName.addEventListener("keyup",()=>{
	console.log(txtFirstName.value);
	spanSelector.innerHTML = `<h1 class="text-warning-">${txtFirstName.value} ${txtLastName.value}</h1>`
})

let txtLastName = document.querySelector("#txt-last-name");

txtLastName.addEventListener("keyup",()=>{
	spanSelector.innerHTML = `<h1 class="text-warning-">${txtFirstName.value} ${txtLastName.value}</h1>`
})

// activity

let dropDownSelector = document.querySelector("select[id]");
	dropDownSelector.addEventListener("change",()=>{

  spanSelector.style.color = `${dropDownSelector.value}`;

});